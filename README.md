# zefie's ABS (Android Build System) Helper

Note that the Android Sources and build environment require at least 140GB of Disk Space, (+ up to 64GB more if using ccache),
as well as at least 16GB RAM. This script will calculate the number of threads to use when building based on the amount of CPUs and System RAM
available, in an attempt to prevent java OOM (out of memory) exceptions while building vital framework files.

**NOTE**: This is a work in progress, do not expect a functional build.

## Getting Started

This script was tested on a fresh ISO install of Ubuntu Server 18.04.x LTS x86_64:

~~~
mkdir -p android/zefie && cd android
wget "https://gitlab.com/rpi4_android/zefie_abs_helper/raw/master/build_android.sh?inline=false" -O zefie/build_android.sh
chmod +x zefie/build_android.sh
zefie/build_android.sh --sync
~~~

This should get you the android source.

## Subcommands

### Standard Subcommands

- `zefie/build_android.sh --sync` syncs the Android Repo Sources
- `zefie/build_android.sh --android` builds the Android OS with ccache. The kernel will also be built, but without ccache. 
- `zefie/build_android.sh --no-ccache --android` builds the Android OS and kernel, without ccache.  
   (**NOTE**: Switching between using ccache and not using ccache will cause the Android Build System to regenerate makefiles, and rebuild some components)
- `zefie/build_android.sh --clean` cleans the Android OS build (warning: will have to rebuild everything, ccache can help speed this up)
- `zefie/build_android.sh --update` updates the zefie script repo only, or just the script if no repo is set up yet.
- `zefie/build_android.sh --help` will show you all supported subcommands, and their short-hand aliases.

### Sudo Subcommands

- `sudo zefie/build_android.sh --partitionsd /dev/sdx` partitions the device on /dev/sdx for Android Pi usage
- `sudo zefie/build_android.sh --flashsd /dev/sdx` installs the images, once the OS and kernel are compiled and the partitions have been created, to /dev/sdx.
- `sudo zefie/build_android.sh --flashsd /dev/sdx bootonly` does the same as above, but skips flashing the system and vendor images.
- `sudo zefie/build_android.sh --flashsd /dev/sdx skipdata` installs all of the images, but does not format userdata. Useful if you want to try to retain your data.

### Subcommand chaining

Subcommands can be chained, an example to update the script, sync the sources, then compile android with ccache.

non-sudo chain:  
`zefie/build_android.sh --update --sync --android`

sudo chain:  
`sudo zefie/build_android.sh --partitionsd /dev/sdx --flashsd /dev/sdx`

You cannot mix sudo and non-sudo subcommands. The chain will stop at the first mismatch.

Subcommands that accept parameters must be closed with `--`, eg `zefie/build_android.sh --kernel menuconfig -- --android make kernel`  
Also note that `--partitionsd` only expects 1 parameter, thus does not need `--`, however since `--flashsd` can accept 2 (device and `bootonly`), you would need to use `--` if not using `bootonly`.

## Device Targets

You can target a specific device, or build type using the `--device` subcommand. This command can be used with or without sudo.  
This subcommand should typically be the first subcommand in the chain if you are not targeting the default (rpi4), **after** any repo, branch, or android version overrides.
The environment is not set to the selected device until the `--device` subcommand is hit in the chain.

  - `zefie/build_android.sh --device <device>` attempts to build an image for the specified device.  
     Any repo overides (listed above) must be placed **BEFORE** the --device subcommand, or they will be ignored.
     Currently Supported Devices:
       - `rpi4` (64-bit multilib rpi4)
       - `rpi4_64` (64-bit only rpi4)
       - `rpi4_swrast` (64-bit multilib rpi4) (Software Rendering)
       - `rpi3` (32-bit only rpi3) (untouched, most likely broken)

## Environment Variable Overrides

You can change some of the script's defaults using environment variables.  

- `Z_JOBS` expects a number, defaults to calculation of (RAM / 3.2GB = Threads), sets the number of threads used when building Android
- `Z_DEVICE` expects a string, is essentially the same as `--device`, and can be used in place of it. Expects the same arugments and will act the same way.
- `KERNEL_MAKE_JOBS` expects a number, defaults to Z_JOBS value, sets number of jobs when building the kernel
- `Z_INCLUDE_ABS_HELPER` accepts any value, defaults to undefined. If defined, will NOT run subcommands, allowing usage of functions in other scripts.

## Developers

  ### Developer commands

  - `zefie/build_android.sh --android-version ...` when used with an uninitialized layout (eg after `--reset-all`) and `--init`, will check out `...` version on Android.
    (eg. `zefie/build_android.sh --android-version android-10.0.0_r25 --init`)

  - `zefie/build_android.sh --local-manifest ...` will use `...` as a local_manifest instead of zefie's repo.
    (eg. `zefie/build_android.sh --local-manifest-url https://gitlab.com/yourprofile/yourrepo` --force-sync)

  - `zefie/build_android.sh --local-manifest-branch ...` will use `...` as a local_manifest branch instead of zefie's default (`android-10`).
    (eg. `zefie/build_android.sh --local-manifest-url https://gitlab.com/yourprofile/yourrepo --local-manifest-branch your-branch --force-sync`)

  - `zefie/build_android.sh --kernel ...` is a workaround to make it easier to configure the kernel. It accepts `make` arguments, but you shouldn't build it with this.
     Instead, use this for commands such `--kernel menuconfig`, `--kernel savedefconfig`, etc. For example, if you were to create a custom RPi4 Kernel configuration based off of mine:  
     - `zefie/build_android.sh --kernel zefie_rpi4_defconfig`
     - `zefie/build_android.sh --kernel menuconfig`
     - (make your changes and save to .config as per normal)
     - `zefie/build_android.sh --kernel savedefconfig`
     - find `out/target/product/rpi4/obj/KERNEL_OBJ/defconfig` and place it in `kernel/rpi/arch/arm64/configs/yourcustom_defconfig` (make sure it ends with `_defconfig`)
     - Enable the new defconfig in the Android device makefile (this guide assumes you already know how to do that)
     <br>
   - `zefie/build_android.sh --android ...` executes ... after running the `source build/envsetup.sh` and `lunch` commands, to run custom commands under 
     the android build environment, without modifying your current environment.  
     (eg `zefie/build_android.sh --android make kernel ramdisk systemimage vendorimage`)

  ### Jenkins

  - #### Flashing from Jenkins without building
    [zefie's Jenkin's server](https://bb.zef.pw/job/android-10_rpi4/) attempts to build the images, and if the build is successful, you can flash it without building Android yourself.     The builds still do not yet work, but if you have a serial console, you can play around and see the errors if you want to try to help us debug the issues.  

    As with `build_android.sh`, subcommands and arguments can be chained.

    By default, `flash_from_jenkins_artifact.sh` will download to wherever `mktemp -d` provides. You can override the temp folder with `Z_JENKINS_TMP`.

    You should also be weary of any partition size changes, as `flash_from_jenkins_artifact.sh` does not yet handle partitioning.

    - `sudo zefie/build_android.sh --partitionsd /dev/sdx` if you have not yet partitioned the SD Card. This does not need to be done between builds unless the partition sizes change.
    - `sudo zefie/flash_from_jenkins_artifact.sh --device /dev/sdx` shows the information for, and offers to flash the latest successful build from Jenkins.
    - `sudo zefie/flash_from_jenkins_artifact.sh --device /dev/sdx --job #` as above, but for a specific build number.
    - `sudo zefie/flash_from_jenkins_artifact.sh --device /dev/sdx --flag flashsd-flag` the `--flag` subcommand accepts the same arguments as the `--flash-sd` overrides, `bootonly` and `skipdata`.
    <br>
  - #### *Frozen Manifests*

    Each time [zefie's Jenkin's server](https://bb.zef.pw/job/android-10_rpi4/) creates a build, it runs `generate_frozen_manifest.sh`, which saves 
    what zefie calls a *frozen manifest*. The script sets all of the revisions in it's current `local_manifests/default.xml` to the commits used at build time. 
    This way you can check out the code as it was when the build was created. This works even if zefie rebases the code, as the commit refs persist in the repository.

    If you would like to sync your code to what was used to compile the build of your choice, you can download the *frozen manifest* from [zefie's Jenkin's server](https://bb.zef.pw/job/android-10_rpi4/). 
    Navigate to the build of your choice, choose "Artifacts", "staging", then "view" next to `frozen_manifest.xml`. Here is an [example](https://bb.zef.pw/job/android-10_rpi4/31/artifact/staging/frozen_manifest.xml/*view*/) of a *frozen manifest*.

    This process is intentionally not automated, as it is unsupported, and you will likely have to manually use 
`${HOME}/bin/repo sync --force-sync` rather than any `--sync` commands in `build_android.sh`, due to it wanting to update to the latest manifest.

## Problem solving

**Use these in the order listed to try to resolve repo sync errors.**

1. If the script is just exiting silently, be sure `Z_INCLUDE_ABS_HELPER` is not set.
   (eg `export Z_INCLUDE_ABS_HELPER=`)

2. If you are getting `*** Please tell me who you are.`, then follow the directions on the screen, and retry. 

3. If the error is regarding an error in `default.xml`, try: `zefie/build_android.sh --reset-local-manifests`  
   to remove, and re-fetch the .repo/local_manifests directory. (In case my rebases break it)

4. If the error is regarding repo sync, and does not mention `zefie/`, try:  
   1. `zefie/build_android.sh --update --force-sync` forces repo to sync, hopefully overriding any conflicts my changes may cause repo to find.
   2. `zefie/build_android.sh --update --clean-sync` to remove all files (except .repo, zefie, and out), and force a re-checkout of all build files (useful to purge all local modifications)
   3. `zefie/build_android.sh --web-update --no-ccache --reset-all` when all hell breaks loose, this will wipe everything except the zefie script, and start over (will have to redownload sources).
   4. `zefie/build_android.sh --web-update --reset-all` same as above, but also runs `ccache -c` (to clears ccache's cache).
   5. `zefie/build_android.sh --init` to re-initialize the repo after a `--reset-all`, but without `--sync`.
   <br>
   
5. If the error is mentioning `zefie/`, try: `zefie/build_android.sh --web-update`  
   to force a web-based script update, to verify you have the latest, then try the above commands again.

If none of this solves the issue, remove the entire project directory and start over.

If you have build errors, make sure you have a **clean** workspace  
(eg `./zefie/build_android.sh --reset-local-manifests --force-sync --clean`).  
If you continue to have build errors, feel free to report it as [a bug of rpi-droid/rpi4](https://gitlab.com/rpi-droid/rpi4/bugs/issues).

## How to build RPi4 Android without this script

1. Install the following packages (or your distro's equivalent):  
   `ccache coreutils build-essential python git bison flex libssl-dev python-mako curl gettext unzip bc libc6-i386 libncurses5-dev`
2. Obtain `repo` from https://source.android.com/source/downloading.html
3. Create and enter project directory
4. ~~~
   repo init -u https://android.googlesource.com/platform/manifest -b android-10.0.0_r25
   git clone https://gitlab.com/rpi4_android/android/local_manifests.git .repo/local_manifests -b android-10
   repo sync
   . build/envsetup.sh
   lunch rpi4-eng
   export KERNEL_MAKE_JOBS=$(nproc)
   make
   ~~~
5. Partition SD Card as follows:
   ~~~
   Partition 1: 128MiB
   Partition 2: 1024MiB
   Partition 3: 128MiB
   Partition 4: (rest of card)
   ~~~
6. Format SD Partition 1 as FAT with label `BOOT` (eg `mkfs.vfat -n BOOT /dev/sdx1`)
7. Copy all contents of `out/target/project/rpi4/bootfiles/` to root of SD Partition 1
8. Copy `out/target/project/rpi4/ramdisk.img` to root of SD Partition 1
9. Write `out/target/project/rpi4/system.img` directly to SD Partition 2 (eg `dd if=out/target/project/rpi4/system.img of=/dev/sdx2 bs=2M`)
10. Write `out/target/project/rpi4/vendor.img` directly to SD Partition 3 (eg `dd if=out/target/project/rpi4/vendor.img of=/dev/sdx3 bs=2M`)
11. Format SD Partition 4 as ext4 with label `userdata` (eg `mkfs.ext4 -L userdata /dev/sdx4`)

**Note**: If passing -j to make, be careful of your system RAM. Allocate roughly 3.2GB of ram per core (so if you have 8 cores and 16GB ram, only use 5 jobs).
Otherwise, during building, java may run out of memory while creating vital framework files, it will say `Killed` on the build log, but will **NOT** trigger a failure!

# Attribution

The repositories used by this script are a collaborative effort between zefie and the [rpi-droid project](https://gitlab.com/rpi-droid).  
For more information about building Android for the Raspberry Pi, please see [the rpi-droid project's documentation](https://rpi-droid.gitlab.io/docs/).

Special thanks to the following for their contributions (in no particular order):  

   - [peyo-hd](https://github.com/peyo-hd)
   - [KonstaT](https://github.com/KonstaT)
   - [RaspberryPiFan](https://gitlab.com/RaspberryPiFan)
   - [brobwind](https://github.com/brobwind)
   - [FydeOS](https://github.com/FydeOS)

# Notes

### Quick rebuild

`./zefie/build_android.sh --update --sync --clean --android`

### Multiple device build (in theory, untested)

`./zefie/build_android.sh --update --sync --device rpi4 --android -- --device rpi3 --android`

