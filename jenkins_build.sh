#!/bin/bash
# shellcheck disable=SC1090

if [ -z "${WORKSPACE}" ]; then
	echo "This script is meant to be run by Jenkins.";
	exit 1;
fi

export Z_INCLUDE_ABS_HELPER=1
Z_SELFPATH="$(realpath "$(dirname "${0}")")"
source "${Z_SELFPATH}/build_android.sh"

cd "${Z_ANDROID}" || exit 1

# Store BUILD_NUMBER in a temp var as to not change Android
# environment, causing it to needlessly regenerate makefiles
export Z_JENKINS_BUILD_NUMBER=${BUILD_NUMBER}
unset BUILD_NUMBER

if [ ! -z "${Z_CLEAN_ON_START}" ]; then
	clean_android_builddir
fi

set_device "${Z_JENKINS_TARGET}"
repo_sync
androenv_exec make

# Restore BUILD_NUMBER
export BUILD_NUMBER=${Z_JENKINS_BUILD_NUMBER}

# Stage if successful
if [ -d "${WORKSPACE}/staging" ]; then
	rm -rf "${WORKSPACE}/staging";
fi

errchk mkdir "${WORKSPACE}/staging";

# Frozen Manifest
errchk ./zefie/generate_frozen_manifest.sh > "${WORKSPACE}/staging/frozen_manifest.xml"

# Log installed files
errchk cp -v "${Z_ANDROID}/${Z_DEVOUT_PATH}/installed-files.txt" "${WORKSPACE}/staging/"

# boot (examples if Z_JENKINS_TARGET=rpi4_swrast)

# ex: /home/user/android/out/target/product/rpi4/boot
errchk cp -rv "${Z_ANDROID}/${Z_DEVOUT_PATH}/boot" "${WORKSPACE}/staging/"

# ex: /home/user/android/out/target/product/rpi4/kernel
errchk cp -v "${Z_ANDROID}/${Z_DEVOUT_PATH}/kernel" "${WORKSPACE}/staging/boot/"

# ex: /home/user/android/out/target/product/rpi4/ramdisk.img
errchk cp -v "${Z_ANDROID}/${Z_DEVOUT_PATH}/ramdisk.img" "${WORKSPACE}/staging/boot/"

# ex: /home/user/android/device/brcm/rpi4/prebuilt/boot
errchk cp -v "${Z_ANDROID}/${Z_DEVICE_PATH}/prebuilt/boot/"* "${WORKSPACE}/staging/boot/"

# ex: /home/user/android/device/brcm/rpi4/rpi4/boot
errchk cp -v "${Z_ANDROID}/${Z_DEVICE_PATH}/${Z_TARGET_DEVICE}/boot/"* "${WORKSPACE}/staging/boot/"

# ex: /home/user/android/device/brcm/rpi4/rpi4_swrast/boot
if [ -d "${Z_ANDROID}/${Z_DEVICE_PATH}/${Z_DEVICE}/boot/" ]; then
	errchk cp -v "${Z_ANDROID}/${Z_DEVICE_PATH}/${Z_DEVICE}/boot/"* "${WORKSPACE}/staging/boot/"
fi

# images
# ex: /home/user/android/device/brcm/rpi4/rpi4/system.img
errchk cp -v "${Z_ANDROID}/${Z_DEVOUT_PATH}/system.img" "${WORKSPACE}/staging/"

# ex: /home/user/android/device/brcm/rpi4/rpi4/vendor.img
errchk cp -v "${Z_ANDROID}/${Z_DEVOUT_PATH}/vendor.img" "${WORKSPACE}/staging/"

if [ ! -z "${Z_CLEAN_ON_SUCCESS}" ]; then
	clean_android_builddir
fi
