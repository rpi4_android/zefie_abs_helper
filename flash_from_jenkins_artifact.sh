#!/bin/bash
# shellcheck disable=SC1090

export Z_INCLUDE_ABS_HELPER=1
Z_SELFPATH="$(realpath "$(dirname "${0}")")"
source "${Z_SELFPATH}/build_android.sh"

Z_JENKINS="https://bb.zef.pw"
Z_JENKINS_JOB="android-10_rpi4"
Z_JENKINS_STAGING="staging"
if [ ! -z "${Z_JENKINS_TMP}" ]; then
	echo "Z_JENKINS_TMP override: ${Z_JENKINS_TMP}"
else
	Z_JENKINS_TMP="$(mktemp -d)"
fi
export Z_JENKINS_TMP

while [ "${1}" != "" ]; do
	case "${1}" in
		"-j"|"--jenkins-build")
			shift
			get_subcmd_args 1 "${@}"
			if [ "${#Z_SUBCMD_ARGS[@]}" -gt 0 ]; then
				shift "${#Z_SUBCMD_ARGS[@]}"
				export Z_JENKINS_BUILD="${Z_SUBCMD_ARGS[*]}"
			fi
			;;

		"-d"|"--device")
			shift
			get_subcmd_args 1 "${@}"
			if [ "${#Z_SUBCMD_ARGS[@]}" -gt 0 ]; then
				shift "${#Z_SUBCMD_ARGS[@]}"
				export Z_FLASH_DEVICE="${Z_SUBCMD_ARGS[*]}"
			fi
			;;

		"-f"|"--flag")
			shift
			get_subcmd_args 1 "${@}"
			if [ "${#Z_SUBCMD_ARGS[@]}" -gt 0 ]; then
				shift "${#Z_SUBCMD_ARGS[@]}"
				export Z_FLASH_CMD="${Z_SUBCMD_ARGS[*]}"
			else
				echo "Usage: --flag [skipdata|bootonly]"
			fi
			;;

		"--")
			continue;
			;;
	esac
done


function set_display_msg() {
	if [ "${Z_JENKINS_BUILD}" == "lastSuccessfulBuild" ]; then
		Z_DISPLAY_MSG="The last succesful build of ${Z_JENKINS_JOB} on Jenkins"
	else
		Z_DISPLAY_MSG="Build #${Z_JENKINS_BUILD} of ${Z_JENKINS_JOB} on Jenkins"
	fi
}

if [ -z "${Z_JENKINS_BUILD}" ]; then
	Z_JENKINS_BUILD="lastSuccessfulBuild"
fi

Z_JENKINS_JOB_URL="${Z_JENKINS}/job/${Z_JENKINS_JOB}/${Z_JENKINS_BUILD}"

function rcopy() {
	local outfile display;
	outfile="$(basename "${1}")"
	if [ ! -z "${2}" ]; then
		display="${2}"
	else
		display="${1}"
	fi

	display+=" from ${Z_DISPLAY_MSG}"

	echo "Copying ${display}..."
	mkdir -p "${Z_JENKINS_TMP}"
	errchk curl -L -o "${Z_JENKINS_TMP}/${outfile}" "${Z_JENKINS_JOB_URL}/artifact/${Z_JENKINS_STAGING}/${1}"
}


if [ -z "${Z_FLASH_DEVICE}" ]; then
	echo "Usage: ${0} -d /dev/sdx [-j jenkins_build]"
	exit 1
fi

errchk silent root_user_chk yes "this command"

Z_REQUIRED_PKGS_SHARED+=(jq)
check_req_pkgs

set_display_msg
echo "Getting information about ${Z_DISPLAY_MSG} ..."
Z_JENKINS_META="$(curl --compressed --silent "${Z_JENKINS_JOB_URL}/api/json")"
Z_JENKINS_BUILD_RESULT="$(echo "${Z_JENKINS_META}" | jq -rM ".result" 2>/dev/null)"
if [ "${Z_JENKINS_BUILD}" == "lastSuccessfulBuild" ]; then
	Z_JENKINS_BUILD_NUMBER_META="$(echo "${Z_JENKINS_META}" | jq -rM ".number" 2>/dev/null)"
	if [ ! -z "${Z_JENKINS_BUILD_NUMBER_META}" ]; then
		Z_JENKINS_BUILD="${Z_JENKINS_BUILD_NUMBER_META}"
		set_display_msg
	fi
fi

case "${Z_JENKINS_BUILD_RESULT}" in
	"SUCCESS")
		;;

	"FAILURE")
		echo "ERROR: ${Z_DISPLAY_MSG} was a failure, so there is nothing to download.";
		exit;
		;;
	"")
		echo "ERROR: ${Z_DISPLAY_MSG} could not be found, so there is nothing to download.";
		exit;
		;;
	*)
		echo "ERROR: ${Z_DISPLAY_MSG} returned an unexpected build result, so will not attempt to download.";
		exit;
		;;
esac
Z_BUILD_DESC="$(echo "${Z_JENKINS_META}" | jq -rM ".description")"
echo -n "${Z_DISPLAY_MSG}"
if [ ! -z "${Z_BUILD_DESC}" ] && [ "${Z_BUILD_DESC}" != "null" ]; then
	echo " has the following description:"
	echo "${Z_BUILD_DESC}"
else
	echo " has no description on file yet."
fi
echo
read -p "Would you like to continue downloading and flashing this build? (y/N)" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]; then
	if [ ! -d "${Z_JENKINS_TMP}" ]; then
		mkdir "${Z_JENKINS_TMP}"
	fi

	set_device "${Z_LOCAL_DEVICE}"
	rcopy "boot/*zip*/boot.zip" "boot files"
	if [ "${Z_FLASH_CMD}" != "bootonly" ]; then
		rcopy "system.img"
		rcopy "vendor.img"
	fi

	flash_sd "${Z_FLASH_DEVICE}" "${Z_FLASH_CMD}"
	# dont run if env var got botched
	if [ ! -z "${Z_JENKINS_TMP}" ]; then
		rm -rf "${Z_JENKINS_TMP}"
	fi
fi

