#!/bin/bash
# shellcheck disable=SC1091
# SC1091 is just a warning telling us it did not check external scripts (eg build/envsetup.sh), so we can safely ignore it.

## Warning! This script assumes a Debian/Fedora Based OS (tested Ubuntu 18.04 x86_64), and that your user has sudo access. ##

# unset Z_DEVICE in case it was set by the environment, because we would assume the other vars were set.
export Z_DEVICE=

Z_ANDROID="$(realpath "$(dirname "${0}")/..")"
Z_SELF_UPDATE_WEB_URL="https://gitlab.com/rpi4_android/zefie_abs_helper/raw/master/build_android.sh?inline=false"
Z_CCACHE_SIZE_MULTIPLIER=0.50 # 50% of free disk space (hey android is big)
Z_GB_RAM_PER_THREAD=3.2
Z_REPO_OPTIMIZATIONS=(--no-clone-bundle --no-tags --current-branch)
Z_REPO_INIT_FLAGS=("${Z_REPO_OPTIMIZATIONS[@]}" --depth 1)
Z_REPO_PATH="${HOME}/bin/repo"
Z_REQUIRED_PKGS_SHARED=(ccache coreutils build-essential python git bison flex libssl-dev python-mako curl gddrescue gettext unzip bc)
Z_REQUIRED_PKGS_DEBIAN=(libc6-i386)
Z_REQUIRED_PKGS_FEDORA=(glibc.i686)
# see --no-ccache subcommand to disable ccache
USE_CCACHE=1
CCACHE_EXEC=$(which ccache)
export CCACHE_EXEC USE_CCACHE

# Hack in additional device targets here, kinda. You are on your own.
Z_BUILD_SUPPORTED=(rpi3 rpi4 rpi4_32 rpi4_64 rpi4_swiftshader rpi4_32_swiftshader)
export Z_BUILD_SUPPORTED

function to_lower() {
	echo "${1}" | tr '[:upper:]' '[:lower:]'
}

function set_device() {

	if [ -z "${1}" ]; then
		# default
		Z_DEVICE="rpi4"
	else
		if [ "$(echo "${1}" | grep '\-' -c )" -gt 0 ]; then
			Z_DEVICE="$(to_lower "${1}" | cut -d'-' -f1)"
			Z_TARGET_TYPE="$(to_lower "${1}" | cut -d'-' -f2)"
		else
			Z_DEVICE="$(to_lower "${1}")"
		fi
	fi

	if [ -z "${Z_TARGET_TYPE}" ]; then
		Z_TARGET_TYPE="eng"
	fi

	case "${Z_DEVICE}" in
		"rpi4"|"rpi4_swiftshader"|"rpi4_64")
			Z_BUILD_ARCH="arm64"
			Z_KERNEL_DEFCONFIG="zefie_rpi4_defconfig"
			Z_BUILD_TARGET="${Z_DEVICE}-${Z_TARGET_TYPE}"
			Z_KERNEL_PATH="kernel/rpi"
			Z_DEVICE_PATH="device/brcm/rpi"
			Z_BOOTP_SIZE="128MiB"
			Z_SYSP_SIZE="1536MiB"
			Z_VENP_SIZE="128MiB"
			Z_KERNEL_CROSS="${Z_ANDROID}/prebuilts/gcc/linux-x86/aarch64/aarch64-linux-android-6.x-ubertc/bin/aarch64-linux-android-"
			Z_KERNEL_CROSS_32="${Z_ANDROID}/prebuilts/gcc/linux-x86/arm/arm-linux-androideabi-6.x-ubertc/bin/arm-linux-androideabi-"
			Z_ANDROVERS="${Z_ANDROVERS:-android-10.0.0_r25}"
			Z_MANIFEST_REPO="${Z_MANIFEST_REPO:-https://gitlab.com/rpi4_android/android/local_manifests.git}"
			Z_MANIFEST_REPO_BRANCH="${Z_MANIFEST_REPO_BRANCH:-android-10}"
			;;
		"rpi4_32"|"rpi4_32_swiftshader")
			Z_BUILD_ARCH="arm"
			Z_KERNEL_DEFCONFIG="zefie_rpi4_defconfig"
			Z_BUILD_TARGET="${Z_DEVICE}-${Z_TARGET_TYPE}"
			Z_KERNEL_PATH="kernel/rpi"
			Z_DEVICE_PATH="device/brcm/rpi"
			Z_BOOTP_SIZE="128MiB"
			Z_SYSP_SIZE="1536MiB"
			Z_VENP_SIZE="128MiB"
			Z_KERNEL_CROSS="${Z_ANDROID}/prebuilts/gcc/linux-x86/arm/arm-linux-androideabi-6.x-ubertc/bin/arm-linux-androideabi"
			Z_KERNEL_CROSS_32=
			Z_ANDROVERS="${Z_ANDROVERS:-android-10.0.0_r25}"
			Z_MANIFEST_REPO="${Z_MANIFEST_REPO:-https://gitlab.com/rpi4_android/android/local_manifests.git}"
			Z_MANIFEST_REPO_BRANCH="${Z_MANIFEST_REPO_BRANCH:-android-10}"
			;;
		"rpi3")
			# completely untested
			Z_BUILD_ARCH="arm"
			Z_KERNEL_DEFCONFIG="lineage_rpi3_defconfig"
			Z_BUILD_TARGET="${Z_DEVICE}-${Z_TARGET_TYPE}"
			Z_KERNEL_PATH="kernel/rpi"
			Z_DEVICE_PATH="device/brcm/rpi3"
			Z_BOOTP_SIZE="128MiB"
			Z_SYSP_SIZE="1024MiB"
			Z_VENP_SIZE="128MiB"
			Z_KERNEL_CROSS="${Z_ANDROID}/prebuilts/gcc/linux-x86/arm/arm-linux-androideabi-6.x-ubertc/bin/arm-linux-androideabi-"
			Z_KERNEL_CROSS_32=
			Z_ANDROVERS="${Z_ANDROVERS:-android-10.0.0_r25}"
			Z_MANIFEST_REPO="${Z_MANIFEST_REPO:-https://gitlab.com/rpi4_android/android/local_manifests.git}"
			Z_MANIFEST_REPO_BRANCH="${Z_MANIFEST_REPO_BRANCH:-android-10}"
			;;
		*)
			echo "WARN: Unsupported device ${Z_DEVICE}"
			exit 1;
	esac

        if [ "$(echo "${Z_DEVICE}" | grep -c '_')" -gt 0 ]; then
                Z_TARGET_DEVICE="$(echo "${Z_DEVICE}" | cut -d'_' -f1)"
        else
                Z_TARGET_DEVICE="${Z_DEVICE}"
        fi

	Z_DEVOUT_PATH="out/target/product/${Z_TARGET_DEVICE}"
	export Z_DEVICE Z_BUILD_ARCH Z_KERNEL_DEFCONFIG Z_BUILD_TARGET \
	       Z_KERNEL_PATH Z_BOOTP_SIZE Z_VENP_SIZE Z_KERNEL_CROSS \
               Z_KERNEL_CROSS_32 Z_DEVOUT_PATH Z_MANIFEST_REPO Z_ANDROVERS \
	       Z_MANIFEST_REPO_BRANCH Z_DEVICE_PATH Z_TARGET_DEVICE Z_TARGET_TYPE;
	echo "*** Current Device Target: ${Z_DEVICE} (Build Type: ${Z_TARGET_TYPE} (${Z_BUILD_ARCH}) ~ Out Dir: out/target/product/${Z_TARGET_DEVICE})"
}

# Note: If compiling for something that is not an rpi, the --partitionsd and --flashsd will be useless
#       but generic compiling may work. Completely untested

# end device config area


function check_device() {
	if [ -z "${Z_DEVICE}" ]; then
		set_device;
	else
		set_device "${Z_DEVICE}"
	fi
}


function root_user_chk() {
	case "${1}" in
		"1"|"yes"|"y")
			if [ "${USER}" != "root" ]; then
				echo "ERROR: You must run ${2} as root!"
				return 1
			fi
			return 0;
			;;

		"0"|"no"|"n")
			if [ "${USER}" == "root" ]; then
				echo "ERROR: Do not run ${2} as root!"
				return 1;
			fi
			return 0;
			;;

		*)
			# invalid option
			return 1;
			;;
	esac
}

function envchkerr() {
	if [ "${1}" -ne 0 ]; then
		echo "Error ${1}";
		exit "${1}";
	fi
}

function errchk() {
	case "${1}" in
		"silent")
			local silent=1
			shift;
			;;
		"noout")
			local noout=1
			shift;
			;;
	esac
	if [ -z "${noout}" ]; then
		"${@}"
	else
		"${@}" 2>/dev/null >/dev/null
	fi
	local res=$?
	if [ ${res} -ne 0 ]; then
		if [ -z "${silent}" ]; then
			echo "Error ${res} executing ${*}";
		fi
		exit "${res}";
	fi
}

function calculate_max_jobs() {
	local nproc
	nproc=$(nproc)
	check_req_pkgs
	if [ ! -z "${USE_CCACHE}" ] && [ ! -z "${CCACHE_EXEC}" ]; then
		if  [ ! -z "${Z_CCACHE_MAX_SIZE}" ]; then
			echo "ccache max size is currently set to ${Z_CCACHE_MAX_SIZE}"
		else
			local ccache_dir ccache_config
			ccache_dir=$("${CCACHE_EXEC}" --print-config | grep 'cache_dir = ' | rev | cut -d' ' -f1 | rev)
			Z_CCACHE_MAX_SIZE=$("${CCACHE_EXEC}" --print-config | grep 'max_size = ' | rev | cut -d' ' -f1 | rev)
			ccache_config=$("${CCACHE_EXEC}" -s | grep 'primary config' | rev | cut -d' ' -f1 | rev)
			Z_CCACHE_SIZE=$(bc <<< "scale=0; $(df -k "${ccache_dir}" | awk ' { print $4 }' | tail -n1)*${Z_CCACHE_SIZE_MULTIPLIER}") # 50% disk space
			if [ ! -f "${ccache_config}" ]; then
				echo "Setting ccache max size to $(bc <<< "scale=2; ${Z_CCACHE_SIZE}/1024/1024")GB..."
				errchk "${CCACHE_EXEC}" --max-size "${Z_CCACHE_SIZE}k"
			else
				echo "ccache max size is currently set to ${Z_CCACHE_MAX_SIZE}"
			fi
		fi
	fi
	if [ -z "${Z_JOBS}" ]; then
		Z_SYS_MEM=$(grep MemTotal /proc/meminfo | rev | cut -d ' ' -f2 | rev)
		Z_MAX_JOBS=$(bc <<< "scale=0; ${Z_SYS_MEM}/${Z_GB_RAM_PER_THREAD}/1024/1024") # RAM in gigs divided by x, rounded, to be safe for java framework
		if [ "${nproc}" -gt "${Z_MAX_JOBS}" ]; then
			Z_JOBS="${Z_MAX_JOBS}";
		else
			Z_JOBS="${nproc}"
		fi
	fi
}

function is_in_array() {
	local e match="$1"
	shift
	for e; do [[ "$e" == "$match" ]] && return 0; done
	return 1
}

function flash_image() {
	echo "Flashing ${1} to ${2}..."
	errchk ddrescue --force -D "${1}" "${2}"
	echo "Syncronizing disks..."
	sync
}

function copy_if_exists() {
	if [ ! -e "${1}" ]; then
		echo "WARN: Could not find ${1}"
		return 1;
	fi;
	local tmpdir;
	tmpdir="${HOME}/tmp";
	if [ ! -d "${tmpdir}" ]; then
		local rmtmpdir=1;
		mkdir "${tmpdir}"
	fi
	local filename dest
	filename="$(basename "${1}")"
	dest="${tmpdir}/${filename}"

	if [ ! -z "${3}" ]; then
		dest="${tmpdir}/${3}"
	fi

#	dest=$(echo "${dest}" | sed 's|//|/|g')
	dest="${dest//\/\//}"

	errchk mount "${2}" "${tmpdir}"
	local mountres=$?
	if [ ${mountres} -eq 0 ]; then
		mkdir -p "$(dirname "${dest}")"
		if [ -d "${1}" ]; then
			cp -vr "${1}"* "${dest}"
		else
			cp -vr "${1}" "${dest}"
		fi
		sync
		umount "${tmpdir}"
	else
		return ${mountres}
	fi
	if [ ! -z "${rmtmpdir}" ]; then
		rmdir "${tmpdir}"
	fi
}

function extract_zip_if_exists() {
	if [ ! -e "${1}" ]; then
		echo "WARN: Could not find ${1}"
		return 1;
	fi;
	# todo: test if actually a zip and fail before errchk does
	local tmpdir;
	tmpdir="${HOME}/tmp";
	if [ ! -d "${tmpdir}" ]; then
		local rmtmpdir=1;
		mkdir "${tmpdir}"
	fi
	local dest
	dest="${tmpdir}/${filename}"

#	dest=$(echo "${dest}" | sed 's|//|/|g')
	dest="${dest//\/\//}"

	errchk mount "${2}" "${tmpdir}"
	local mountres=$?
	if [ ${mountres} -eq 0 ]; then
		mkdir -p "$(dirname "${dest}")"
		errchk unzip "${1}" -d "${dest}"
		sync
		umount "${tmpdir}"
	else
		return ${mountres}
	fi
	if [ ! -z "${rmtmpdir}" ]; then
		rmdir "${tmpdir}"
	fi
}


function check_packages_fedora() {
	if [ -z "${Z_PKG_LIST}" ]; then
		Z_PKG_LIST=$(rpm -qa);
	fi

	echo "${Z_PKG_LIST}" | grep -c "${1}"
}

function check_packages_debian() {
	if [ -z "${Z_PKG_LIST}" ]; then
		Z_PKG_LIST=$(dpkg --list);
	fi

	echo "${Z_PKG_LIST}" | grep -c "^ii  ${1}[ \:]"
}

function update_script() {
	cd "${Z_ANDROID}" || return 1
	check_req_pkgs
	if [ -d "${Z_ANDROID}/zefie/.git" ] && [ "${1}" != "web" ]; then
		echo "Updating zefie's helper script via repo..."
		# remove self to prevent any sync issues from web updates, edits, etc
		rm -f "${0}"
		errchk "${Z_REPO_PATH}" sync "${Z_REPO_SYNC_FLAGS[@]}" zefie
		cd "${Z_ANDROID}/zefie" || return 1
		errchk noout git reset --hard
		cd "${Z_ANDROID}" || return 1
	else
		# fallback to web url update
		echo "Updating zefie's helper script via web..."
		errchk curl -L "${Z_SELF_UPDATE_WEB_URL}" > "${0}"
	fi
}

function check_req_pkgs() {
	Z_INSTALL_PKGS=()
	local req_pkg+=("${Z_REQUIRED_PKGS_SHARED[@]}");
	if ! [ "$(command -v apt)" == "" ]; then
		req_pkg+=("${Z_REQUIRED_PKGS_DEBIAN[@]}")
	    	for pkg in "${req_pkg[@]}"; do
	    		if [ "$(check_packages_debian "${pkg}")" -eq 0 ]; then
	    			Z_INSTALL_PKGS+=("${pkg}")
	    		fi
	    	done
	elif  ! [ "$(command -v rpm)" == "" ]; then
		req_pkg+=("${Z_REQUIRED_PKGS_FEDORA[@]}")
	    	for pkg in "${req_pkg[@]}"; do
	    		if [ "$(check_packages_fedora "${pkg}")" -eq 0 ]; then
	    			Z_INSTALL_PKGS+=("${pkg}")
	    		fi
	        done
	fi

	if [ ${#Z_INSTALL_PKGS[@]} -gt 0 ]; then
		echo "We need to install some additional packages: ${Z_INSTALL_PKGS[*]}"
			if ! [ "$(command -v apt)" == "" ]; then
			errchk sudo apt-get install -y "${Z_INSTALL_PKGS[@]}"
			fi
	elif  ! [ "$(command -v dnf)" == "" ]; then
			errchk sudo dnf install -y "${Z_INSTALL_PKGS[@]}"
	fi

	# check for condition where ccache was not installed at script startup, thus CCACHE_EXEC is empty
	if [ ! -z "${USE_CCACHE}" ] && [ -z "${CCACHE_EXEC}" ]; then
		CCACHE_EXEC=$(which ccache)
		export CCACHE_EXEC
	fi
}


function androenv_exec() {
	check_device
	calculate_max_jobs
	source build/envsetup.sh
	envchkerr $?
	lunch "${Z_BUILD_TARGET}"
	envchkerr $?
	if [ -z "${KERNEL_MAKE_JOBS}" ]; then
		export KERNEL_MAKE_JOBS=${Z_JOBS}
	fi
	echo "KERNEL_MAKE_JOBS=${KERNEL_MAKE_JOBS}"
	if [ "${1}" == "make" ]; then
		# force our job count, if the user specifies -j, it will override this anyway, since it will come after ours
		shift;
		errchk make "-j${Z_JOBS}" "${@}"
	else
		errchk "${@}"
	fi
}

function partition_sd() {
	check_device
	if [ "$(echo "${1}" | grep -c '/sda')" -ge 1 ]; then
		echo "Refuse to work on ${1}!"
		exit 1;
	fi

	if [ "$(grep -c "${1}" /proc/mounts)" -ge 1 ]; then
		echo "Refuse to work on device with mounted partitions ${1}!"
		exit 1;
	fi

	if [ "${USER}" != "root" ]; then
		echo "root required, try again with sudo"
		exit 1;
	fi

	errchk fdisk "${1}" <<EEOF
o
n
p
1

+${Z_BOOTP_SIZE}
n
p
2

+${Z_SYSP_SIZE}
n
p
3

+${Z_VENP_SIZE}
n
p


a
1
t
1
c
w
EEOF
}

function flash_sd() {
	check_device
	errchk mkfs.vfat -n "BOOT" "${1}1"
	if [ -z "${Z_JENKINS_TMP}" ]; then
		copy_if_exists "${Z_ANDROID}/${Z_DEVOUT_PATH}/boot/" "${1}1" "/"
		copy_if_exists "${Z_ANDROID}/${Z_DEVOUT_PATH}/kernel" "${1}1" "/"
		copy_if_exists "${Z_ANDROID}/${Z_DEVICE_PATH}/prebuilt/boot/" "${1}1" "/"
		copy_if_exists "${Z_ANDROID}/${Z_DEVICE_PATH}/${Z_DEVICE}/boot/" "${1}1" "/"
		copy_if_exists "${Z_ANDROID}/${Z_DEVOUT_PATH}/ramdisk.img" "${1}1"
		if [ "${2}" != "bootonly" ]; then
			if [ -f "${Z_ANDROID}/${Z_DEVOUT_PATH}/system.img" ]; then
				flash_image "${Z_ANDROID}/${Z_DEVOUT_PATH}/system.img" "${1}2"
			fi
			if [ -f "${Z_ANDROID}/${Z_DEVOUT_PATH}/vendor.img" ]; then
				flash_image "${Z_ANDROID}/${Z_DEVOUT_PATH}/vendor.img" "${1}3"
			fi
			if [ "${2}" != "skipdata" ]; then
				errchk mkfs.ext4 -FL "userdata" "${1}4"
			fi
		fi
	else
		echo "Extracting boot files to ${1}1 ..."
		extract_zip_if_exists "${Z_JENKINS_TMP}/boot.zip" "${1}1"
		if [ "${2}" != "bootonly" ]; then
			if [ -f "${Z_JENKINS_TMP}/system.img" ]; then
				flash_image "${Z_JENKINS_TMP}/system.img" "${1}2"
			fi
			if [ -f "${Z_JENKINS_TMP}/vendor.img" ]; then
				flash_image "${Z_JENKINS_TMP}/vendor.img" "${1}3"
			fi
			if [ "${2}" != "skipdata" ]; then
				errchk mkfs.ext4 -FL "userdata" "${1}4"
			fi
		fi
	fi
	echo "Syncronizing disks..."
	sync
}

function reset_local_manifests() {
	local startdir
	startdir="$(pwd)"
	check_device
	if [ -d "${Z_ANDROID}/.repo/local_manifests" ]; then
		rm -rf "${Z_ANDROID}/.repo/local_manifests"
	fi
	cd "${Z_ANDROID}/.repo" || return 1
	errchk git clone "${Z_MANIFEST_REPO}" "${Z_ANDROID}/.repo/local_manifests" -b "${Z_MANIFEST_REPO_BRANCH}"
	cd "${startdir}" || return 1
}

function clean_android_builddir() {
	local startdir
	startdir="$(pwd)"
	echo "Cleaning build directory..."
	cd "${Z_ANDROID}" || return 1
	errchk make clean
	cd "${startdir}" || return 1
}

function init_android_repo() {
	local startdir
	startdir="$(pwd)"
	check_device
	cd "${Z_ANDROID}" || return 1
	echo "Attempting to initialize Android Source Repo..."
	errchk "${Z_REPO_PATH}" init -u https://android.googlesource.com/platform/manifest -b "${Z_ANDROVERS}" "${Z_REPO_INIT_FLAGS[@]}"
	cd "${startdir}" || return 1
}

function wipe_project_dir() {
	local startdir
	startdir="$(pwd)"
	cd "${Z_ANDROID}" || return 1
	for f in *; do
		if is_in_array "${@}" "${f}"; then
			continue;
		fi;
		echo "Wiping ${f}...";
		rm -rf "${f}";
	done
	cd "${startdir}" || return 1
}

function repo_sync() {
	local startdir
	startdir="$(pwd)"
	check_device
	calculate_max_jobs
	if [ ! -e "${Z_REPO_PATH}" ]; then
		if [ ! -d "${HOME}/bin" ]; then mkdir "${HOME}/bin"; fi
		echo "Installing \"repo\" command to ${Z_REPO_PATH} ...";
		errchk curl https://storage.googleapis.com/git-repo-downloads/repo > "${Z_REPO_PATH}"
		errchk chmod +x "${Z_REPO_PATH}"
	fi
	if [ ! -d "${Z_ANDROID}/.repo" ]; then
		init_android_repo
	else
		cd "${Z_ANDROID}" || return 1
	fi

	echo "Syncing Android Local Manifest..."
	if [ -d "${Z_ANDROID}/.repo/local_manifests/.git" ]; then
		cd "${Z_ANDROID}/.repo/local_manifests" || return 1
		errchk git pull
	else
		reset_local_manifests
	fi

	cd "${Z_ANDROID}" || return 1
	Z_REPO_SYNC_FLAGS=("${Z_REPO_OPTIMIZATIONS[@]}" "-j${Z_JOBS}")
	if [ "${1}" == "clean" ] || [ "${1}" == "force" ]; then
		if [ "${1}" == "clean" ]; then
			echo "Preparing source tree for re-checkout..."
			wipe_project_dir out zefie
		fi
		Z_REPO_SYNC_FLAGS+=(--force-sync --force-remove-dirty --detach)
	fi
	echo "Syncing Android Source using ${Z_JOBS} threads..."
	errchk "${Z_REPO_PATH}" sync "${Z_REPO_SYNC_FLAGS[@]}"
	cd "${startdir}" || return 1
}

function show_help() {
	echo "Usage: ${0} [--sync] [--update] [--clean] [--android] [--partsd] [--flashsd]"
	echo "============================================================================"
	echo "Please see https://gitlab.com/rpi4_android/zefie_abs_helper for more documentation."
}

function get_subcmd_args() {
	local arg argcnt max_args;
	max_args=${1}
	shift
	Z_SUBCMD_ARGS=();
	argcnt=${#}
	for f in $(seq 1 "$((argcnt+1))"); do
		arg="${!f}"
		if [ "${arg}" == "--" ] || (  [ "${max_args}" -gt 0 ] && [ ${#Z_SUBCMD_ARGS[@]} -ge "${max_args}" ] ); then
			break;
		fi
		Z_SUBCMD_ARGS+=(${!f})
	done
}


function kenv_make() {
	local startdir
	startdir="$(pwd)"
	if [ "${1}" == "menuconfig" ]; then
		Z_REQUIRED_PKGS_SHARED+=(libncurses5-dev)
	fi
	check_device
	calculate_max_jobs
	cd "${Z_ANDROID}" || return 1

	local Z_PREV_PATH;
	Z_PREV_PATH="${PATH}"
	PATH="${Z_ANDROID}/prebuilts/gcc/linux-x86/host/x86_64-linux-glibc2.17-4.8/x86_64-linux/bin:$PATH"
	export PATH
	local KENV=( "-j${Z_JOBS}" "CFLAGS_MODULE=-fno-pic" -C "${Z_ANDROID}/${Z_KERNEL_PATH}" \
                     "O=${Z_ANDROID}/${Z_DEVOUT_PATH}/obj/KERNEL_OBJ" "ARCH=${Z_BUILD_ARCH}" "CC=${Z_ANDROID}/prebuilts/clang/host/linux-x86/clang-r353983c/bin/clang" \
                     "CLANG_TRIPLE=aarch64-linux-gnu" "HOSTCC=${Z_ANDROID}/prebuilts/clang/host/linux-x86/clang-r353983c/bin/clang" \
                     "HOSTAR=${Z_ANDROID}/prebuilts/gcc/linux-x86/host/x86_64-linux-glibc2.17-4.8/x86_64-linux/bin/ar" \
                     "HOSTLD=${Z_ANDROID}/prebuilts/gcc/linux-x86/host/x86_64-linux-glibc2.17-4.8/x86_64-linux/bin/ld" \
                     "HOSTCXX=${Z_ANDROID}/prebuilts/clang/host/linux-x86/clang-r353983c/bin/clang++" "PERL=$(which perl)" \
                     "BISON=$(which bison)" "FLEX=$(which flex)" "CROSS_COMPILE=${Z_KERNEL_CROSS}" )

	if [ "${Z_BUILD_ARCH}" == "arm64" ]; then
		KENV+=(	"CROSS_COMPILE_ARM32=${Z_ANDROID}/${Z_KERNEL_CROSS_32}" )
	fi

	errchk "${Z_ANDROID}/prebuilts/build-tools/linux-x86/bin/make" "${KENV[@]}" -f "${Z_ANDROID}/${Z_KERNEL_PATH}/Makefile.orig" "${@}"

	PATH="${Z_PREV_PATH}"
	export PATH
	cd "${startdir}" || return 1
}


if [ -z "${Z_INCLUDE_ABS_HELPER}" ]; then
	while [ "${1}" != "" ]; do
		case "${1}" in
			"-a"|"--android")
				errchk silent root_user_chk no "${1}"
				shift
				get_subcmd_args 0 "${@}"
				if [ "${#Z_SUBCMD_ARGS[@]}" -gt 0 ]; then
					shift "${#Z_SUBCMD_ARGS[@]}"
					androenv_exec "${Z_SUBCMD_ARGS[@]}"
				else
					androenv_exec make
				fi
				;;

			"-k"|"--kernel")
				errchk silent root_user_chk no "${1}"
				shift
				get_subcmd_args 0 "${@}"
				if [ "${#Z_SUBCMD_ARGS[@]}" -gt 0 ]; then
					shift "${#Z_SUBCMD_ARGS[@]}"
					kenv_make "${Z_SUBCMD_ARGS[@]}"
				else
					echo "Usage ${0} --kernel [make arguments]"
				fi
				;;

			"-d"|"--dev"|"--device"|"--target-device")
				# don't care if root or not, so no check
				shift
				get_subcmd_args 1 "${@}"
				if [ "${#Z_SUBCMD_ARGS[@]}" -gt 0 ]; then
					shift "${#Z_SUBCMD_ARGS[@]}"
					set_device "${Z_SUBCMD_ARGS[@]}"
				else
					echo "Usage ${0} --device [build_target]"
					echo "Currently supported build targets: ${Z_BUILD_SUPPORTED[*]}"
					exit 1;
				fi
				;;

			"-v"|"--vers"|"--version"|"--android-version")
				# don't care if root or not, so no check
				shift
				get_subcmd_args 1 "${@}"
				if [ "${#Z_SUBCMD_ARGS[@]}" -gt 0 ]; then
					shift "${#Z_SUBCMD_ARGS[@]}"
					export Z_ANDROVERS="${Z_SUBCMD_ARGS[*]}"
				else
					echo "Usage ${0} --android-version [branch_from_aosp_repos]"
					exit 1;
				fi
				;;

			"-ru"|"-lm"|"--repo"|"--repo-url"|"--local-manifest"|"--local-manifest-url")
				# don't care if root or not, so no check
				shift
				get_subcmd_args 1 "${@}"
				if [ "${#Z_SUBCMD_ARGS[@]}" -gt 0 ]; then
					shift "${#Z_SUBCMD_ARGS[@]}"
					export Z_MANIFEST_REPO="${Z_SUBCMD_ARGS[*]}"
				else
					echo "Usage ${0} --local-manifest [git_url_to_local_manifests]"
					exit 1;
				fi
				;;

			"-rb"|"-lmb"|"--branch"|"--local-manifest-branch")
				# don't care if root or not, so no check
				shift
				get_subcmd_args 1 "${@}"
				if [ "${#Z_SUBCMD_ARGS[@]}" -gt 0 ]; then
					shift "${#Z_SUBCMD_ARGS[@]}"
					export Z_MANIFEST_REPO_BRANCH="${Z_SUBCMD_ARGS[*]}"
				else
					echo "Usage ${0} --local-manifest-branch [branch_from_local_manifests_repo]"
					exit 1;
				fi
				;;

			"-nc"|"--no-ccache"|"--disable-ccache")
				shift
				if is_in_array "${Z_REQUIRED_PKGS_SHARED[@]}" ccache; then
					Z_REQUIRED_PKGS_SHARED=("${Z_REQUIRED_PKGS_SHARED[@]:1}")
				fi
				unset USE_CCACHE
				unset CCACHE_EXEC
				;;

			"-c"|"--clean")
				errchk silent root_user_chk no "${1}"
				shift
				clean_android_builddir
				;;

			"-s"|"--sync")
				errchk silent root_user_chk no "${1}"
				shift
				repo_sync
				;;

			"-cs"|"--clean-sync")
				errchk silent root_user_chk no "${1}"
				shift
				repo_sync clean
				;;

			"-fs"|"--force-sync")
				errchk silent root_user_chk no "${1}"
				shift
				repo_sync force
				;;

			"-i"|"--init")
				errchk silent root_user_chk no "${1}"
				shift
				init_android_repo
				;;

			"-u"|"--update")
				update_script
				shift
				if [ ! -z "${1}" ]; then
					echo "Restarting with updated script..."
					"${0}" "${@}"
				fi
				exit
				;;

			"-wu"|"--web-update")
				update_script web
				shift
				if [ ! -z "${1}" ]; then
					echo "Restarting with updated script..."
					"${0}" "${@}"
				fi
				exit
				;;

			"-r"|"--reset-local-manifests")
				errchk silent root_user_chk no "${1}"
				shift
				reset_local_manifests
				;;

			"-ra"|"--reset-all")
				errchk silent root_user_chk no "${1}"
				shift
				wipe_project_dir zefie
				echo "Wiping .repo..."
				rm -rf .repo
				init_android_repo
				reset_local_manifests
				if [ ! -z "${CCACHE_EXEC}" ]; then
					errchk "${CCACHE_EXEC}" --clear
				fi
				;;

			"-p"|"--partitionsd"|"--partsd")
				errchk silent root_user_chk yes "${1}"
				shift
				get_subcmd_args 1 "${@}"
				if [ "${#Z_SUBCMD_ARGS[@]}" -gt 0 ]; then
					shift "${#Z_SUBCMD_ARGS[@]}"
					partition_sd "${Z_SUBCMD_ARGS[@]}"
				else
					echo "Usage ${0} --partitionsd /dev/device"
					exit 1;
				fi
				;;
			"-f"|"--flashsd")
				errchk silent root_user_chk yes "${1}"
				shift
				get_subcmd_args 2 "${@}"
				if [ "${#Z_SUBCMD_ARGS[@]}" -gt 0 ]; then
					shift "${#Z_SUBCMD_ARGS[@]}"
					flash_sd "${Z_SUBCMD_ARGS[@]}"
				else
					echo "Usage ${0} --flashsd /dev/device"
					exit 1;
				fi
				;;

			"-h"|"-?"|"--help")
				show_help
				echo " "
				echo "Subcommands may have shorter aliases, here are some examples:"
				grep "\"\-\-" "${0}" | grep ')$' | awk ' { print $1 } ' | grep -v "\"--\"" | cut -d')' -f1 | sed "s/|/ /g" | sed 's/"//g'
				exit
				;;

			"--")
				continue;
				;;

			*)
				show_help
				exit 1;
		esac
	done
fi
