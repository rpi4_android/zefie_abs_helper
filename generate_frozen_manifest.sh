#!/bin/bash
# shellcheck disable=SC1090,SC2001

export Z_INCLUDE_ABS_HELPER=1
Z_SELFPATH="$(realpath "$(dirname "${0}")")"
source "${Z_SELFPATH}/build_android.sh"

function get_commit() {
	local pwd;
	pwd="$(pwd)"
	cd "${1}" || exit 1
	git log HEAD | head -n1 | cut -d' ' -f2
	cd "${pwd}" || exit 1
}


cd "${Z_ANDROID}" || exit 1
Z_MANIFEST=".repo/local_manifests/default.xml"

while IFS=$'\n' read -r l; do
	if [ ! -z "${Z_IN_COMMENT_LOOP}" ]; then
		if [ "$(echo "${l}" | grep "\-\->" -c)" -ge 1 ]; then
			Z_IN_COMMENT_LOOP=;
		fi
		echo "${l}";
		continue;
	fi
	if [ "$(echo "${l}" | grep "<!--" -c)" -ge 1 ]; then
		if [ "$(echo "${l}" | grep "\-\->" -c)" -eq 0 ]; then
			Z_IN_COMMENT_LOOP=1;
		fi
		echo "${l}";
		continue;
	fi
	if [ "$(echo "${l}" | grep "<project" -c)" -ge 1 ]; then
		if [ "$(echo "${l}" | grep fetch -c)" -ge 1 ] ; then
			echo "${l}";
			continue;
		fi

		Z_GIT_PATH=$(echo "${l}" | grep -o 'path=.*' | cut -d'"' -f2)
		Z_GIT_COMMIT=$(get_commit "${Z_GIT_PATH}")
		echo "${l}" | sed "s|revision=\".*\"|revision=\"${Z_GIT_COMMIT}\"|g"
	else
		echo "${l}";
		continue;
	fi
done < "${Z_MANIFEST}";
